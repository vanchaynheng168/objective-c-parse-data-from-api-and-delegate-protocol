//
//  Book.h
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 5/5/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Book : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *author;

@end

NS_ASSUME_NONNULL_END
