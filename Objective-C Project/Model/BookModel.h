//
//  BookModel.h
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 7/5/21.
//

#import <JSONModel/JSONModel.h>

//NS_ASSUME_NONNULL_BEGIN

@interface AbbrevModel : JSONModel
@property (nonatomic) NSString *pt;
@property (nonatomic) NSString *en;
@end


@interface BookModel : JSONModel
@property (nonatomic) AbbrevModel *abbrev;
@property (nonatomic) NSString *author;
@property (nonatomic) NSString *chapters;
@property (nonatomic) NSString *group;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *testament;
@end


//NS_ASSUME_NONNULL_END


