//
//  NavViewController.h
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 7/5/21.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NavViewController : UIViewController<DelegateData>
@property (nonatomic, assign) NSString *str;
@property (strong, nonatomic) IBOutlet UILabel *lable;

@end

NS_ASSUME_NONNULL_END
