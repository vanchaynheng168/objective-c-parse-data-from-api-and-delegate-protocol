//
//  ViewController.h
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 4/5/21.
//

#import <UIKit/UIKit.h>

@protocol DelegateData <NSObject>   //define delegate protocol
    - (void) sentDataMethod: (NSString *)sender;  //define delegate method to be implemented within another class

@end //end protocol

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

@property (nonatomic, weak) id <DelegateData> delegate; //define MyClassDelegate as delegate

@end

