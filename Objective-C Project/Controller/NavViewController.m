//
//  NavViewController.m
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 7/5/21.
//

#import "NavViewController.h"
#import "ViewController.h"

@interface NavViewController ()

@property (strong, nonatomic) NSString *name;

@end

@implementation NavViewController
 
- (void)viewDidLoad {
    [super viewDidLoad];
    self.lable.text = self.name;
   
    
    // Do any additional setup after loading the view.
}

- (void)sentDataMethod:(NSString *)sender {
    NSLog(@"%@", sender);
    self.name = sender;
}

@end
