//
//  ViewController.m
//  Objective-C Project
//
//  Created by Nheng Vanchhay on 4/5/21.
//

#import "ViewController.h"
#import "Book.h"
#import "NavViewController.h"
#import "BookModel.h"


@interface ViewController ()

@property (strong, nonatomic) NSMutableArray<Book *> *books;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Article";
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    
    // way to call func objective-c
    [self setupArticle];
    
}

- (IBAction)pushScreeen:(UIBarButtonItem *)sender {
    
    NavViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"vc"];
    vc.str = @"YES"; // pass data by class property
    self.delegate = vc;
    [self.delegate sentDataMethod:@"Nav work"]; //delegate data
    [self.navigationController pushViewController:vc animated:true];
    
}

- (void) setupArticle {
    
    self.books = NSMutableArray.new;
    
    [self fetchData];
}

// fetch data from free api
- (void) fetchData {
    
    NSString *urlString = @"https://www.abibliadigital.com.br/api/books";
    NSURL *url = [NSURL URLWithString:urlString];

    [[NSURLSession.sharedSession dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError *err;
        
        NSArray *bookJson = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&err];
        
        NSLog(@"%@ hhhee: ",bookJson);
        if (err) {
            NSLog(@"Cann't convert data to Dict");
        }
        
        for( NSDictionary *jsonDict in bookJson) {
//            NSLog(@"%@", jsonDict[@"author"]);
            
            NSString *name      = jsonDict[@"name"];
            NSString *author    = jsonDict[@"author"];
            
            //init new obj of array
            Book *book = Book.new;
            
            //pass data to custom property
            book.name           = name;
            book.author         = author;
            
            // add data to array
            [self.books addObject:book];
            
            //async data
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.myTableView reloadData];
            });
        }
    }] resume];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"cell" forIndexPath:indexPath];
    cell.textLabel.text         = self.books[indexPath.row].name;
    cell.detailTextLabel.text   = self.books[indexPath.row].author;
    return  cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.books.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NavViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"vc"];
    self.delegate = vc;
    [self.delegate sentDataMethod:self.books[indexPath.row].name]; //delegate data
    [self.navigationController pushViewController:vc animated:true];
}

@end

